webpackJsonp([1,4],{

/***/ 145:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Paper; });
/**
 * Summary:
 * Model which describes Paper entity in paper management
 */
var Paper = (function () {
    function Paper(id, title, author, grade, summary, dateUpload, file, topicId) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.grade = grade;
        this.summary = summary;
        this.dateUpload = dateUpload;
        this.file = file;
        this.topicId = topicId;
    }
    return Paper;
}());
//# sourceMappingURL=D:/STUDENT/WROCLAW/Semestr_2/Software System Development/implementation/yams/src/Paper.js.map

/***/ }),

/***/ 351:
/***/ (function(module, exports) {

module.exports = "/**\r\n* Summary:\r\n* General styles for PaperManagement\r\n*/\n.paperGeneral__header {\n  width: 100%;\n  height: 150px;\n  padding: 20px 50px;\n  margin-bottom: 5px;\n  background-color: #fcfcfc;\n  border-bottom: solid #ff3333 3px;\n  box-shadow: black 0px 2px 10px; }\n\n.paperGeneral__frame {\n  width: 100%;\n  padding: 20px 50px;\n  background-color: #f2f2f2; }\n\n.paperGeneral__paperFrame {\n  border: solid black 2px;\n  width: 80%;\n  height: 480px;\n  padding: 10px; }\n\n.paperGeneral__reviewPanel {\n  position: fixed;\n  top: 150px;\n  right: 0;\n  border: solid black 2px;\n  border-right: none;\n  height: 400px;\n  width: 20%;\n  margin-left: 50px;\n  margin-top: 20px;\n  padding: 5px 10px;\n  text-align: center;\n  overflow: hidden; }\n\n.paperGeneral__reviewPaper {\n  display: inline-block;\n  margin: 20px; }\n\n.paperGeneral__icon {\n  border-radius: 50%;\n  width: 80px;\n  height: 80px; }\n\n.paperGeneral__searchResults {\n  width: 100%;\n  height: 300px;\n  padding: 10px;\n  overflow: auto;\n  border: solid black 2px; }\n"

/***/ }),

/***/ 352:
/***/ (function(module, exports) {

function webpackEmptyContext(req) {
	throw new Error("Cannot find module '" + req + "'.");
}
webpackEmptyContext.keys = function() { return []; };
webpackEmptyContext.resolve = webpackEmptyContext;
module.exports = webpackEmptyContext;
webpackEmptyContext.id = 352;


/***/ }),

/***/ 353:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(440);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__environments_environment__ = __webpack_require__(468);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_app_module__ = __webpack_require__(461);




if (__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].production) {
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["a" /* enableProdMode */])();
}
__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */]);
//# sourceMappingURL=D:/STUDENT/WROCLAW/Semestr_2/Software System Development/implementation/yams/src/main.js.map

/***/ }),

/***/ 460:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AppComponent = (function () {
    function AppComponent() {
    }
    AppComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["U" /* Component */])({
            selector: 'app-root',
            template: __webpack_require__(627),
            styles: [__webpack_require__(622)]
        }), 
        __metadata('design:paramtypes', [])
    ], AppComponent);
    return AppComponent;
}());
//# sourceMappingURL=D:/STUDENT/WROCLAW/Semestr_2/Software System Development/implementation/yams/src/app.component.js.map

/***/ }),

/***/ 461:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(431);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(296);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_component__ = __webpack_require__(460);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__components_PaperManagement_PaperGeneral_component__ = __webpack_require__(465);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__components_PaperManagement_PaperGeneralFrame_component__ = __webpack_require__(466);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__components_PaperManagement_PaperDetails_PaperDetails_component__ = __webpack_require__(463);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__components_PaperManagement_PaperAdd_PaperAdd_component__ = __webpack_require__(462);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__components_PaperManagement_PaperEdit_PaperEdit_component__ = __webpack_require__(464);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__components_PaperManagement_PaperReview_PaperReview_component__ = __webpack_require__(467);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__services_PaperManagement_service__ = __webpack_require__(97);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};












var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["b" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_5__components_PaperManagement_PaperGeneral_component__["a" /* PaperGeneralComponent */],
                __WEBPACK_IMPORTED_MODULE_6__components_PaperManagement_PaperGeneralFrame_component__["a" /* PaperGeneralFrameComponent */],
                __WEBPACK_IMPORTED_MODULE_7__components_PaperManagement_PaperDetails_PaperDetails_component__["a" /* PaperDetailsComponent */],
                __WEBPACK_IMPORTED_MODULE_8__components_PaperManagement_PaperAdd_PaperAdd_component__["a" /* PaperAddComponent */],
                __WEBPACK_IMPORTED_MODULE_9__components_PaperManagement_PaperEdit_PaperEdit_component__["a" /* PaperEditComponent */],
                __WEBPACK_IMPORTED_MODULE_10__components_PaperManagement_PaperReview_PaperReview_component__["a" /* PaperReviewComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* HttpModule */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_11__services_PaperManagement_service__["a" /* PaperManagementService */]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* AppComponent */]]
        }), 
        __metadata('design:paramtypes', [])
    ], AppModule);
    return AppModule;
}());
//# sourceMappingURL=D:/STUDENT/WROCLAW/Semestr_2/Software System Development/implementation/yams/src/app.module.js.map

/***/ }),

/***/ 462:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__models_Paper__ = __webpack_require__(145);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__models_PageMode__ = __webpack_require__(96);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_PaperManagement_service__ = __webpack_require__(97);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PaperAddComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var PaperAddComponent = (function () {
    //TODO: get all papers uploaded for this conference
    function PaperAddComponent(_paperMngService) {
        this._paperMngService = _paperMngService;
        this.currDate = new Date();
        this.changePageMode = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["G" /* EventEmitter */]();
    }
    PaperAddComponent.prototype.ngOnInit = function () {
        //TODO: implement request inside service
        //this.uploadHistory = this._paperMngService.getUploadHistoryHttp();
        this.uploadHistory = [];
        //TODO: get from service
        this.topics = [{ id: 1, name: "Science" }, { id: 2, name: "Fiction" }];
        //init new paper creation
        this.newPaper = new __WEBPACK_IMPORTED_MODULE_1__models_Paper__["a" /* Paper */](-1, '', '', -1, '', (new Date().toISOString()), null, -1);
        this._fileListOrig = this.fileInput.nativeElement.files;
    };
    PaperAddComponent.prototype.backToGeneral = function () {
        this.changePageMode.emit(__WEBPACK_IMPORTED_MODULE_2__models_PageMode__["a" /* PageMode */].General);
    };
    PaperAddComponent.prototype.addNewPaper = function (newPaper) {
        this._paperMngService.addNewPaperHttp(newPaper);
    };
    PaperAddComponent.prototype.loadFile = function (event) {
        this.newPaper.file = event.target.files[0];
    };
    PaperAddComponent.prototype.removeFile = function () {
        this.fileInput.nativeElement.files = this._fileListOrig;
        this.newPaper.file = null;
    };
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["V" /* ViewChild */])('fileInput'), 
        __metadata('design:type', Object)
    ], PaperAddComponent.prototype, "fileInput", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["T" /* Output */])('changePageMode'), 
        __metadata('design:type', Object)
    ], PaperAddComponent.prototype, "changePageMode", void 0);
    PaperAddComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["U" /* Component */])({
            selector: 'paper-add',
            template: __webpack_require__(628),
            styles: [__webpack_require__(623)]
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_3__services_PaperManagement_service__["a" /* PaperManagementService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3__services_PaperManagement_service__["a" /* PaperManagementService */]) === 'function' && _a) || Object])
    ], PaperAddComponent);
    return PaperAddComponent;
    var _a;
}());
//# sourceMappingURL=D:/STUDENT/WROCLAW/Semestr_2/Software System Development/implementation/yams/src/PaperAdd.component.js.map

/***/ }),

/***/ 463:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__models_Paper__ = __webpack_require__(145);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PaperDetailsComponent; });
/**
 * Summary:
 * Component to display each paper card on the search list
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PaperDetailsComponent = (function () {
    function PaperDetailsComponent() {
        this._editPaperEvent = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["G" /* EventEmitter */]();
    }
    PaperDetailsComponent.prototype.editPaper = function (paper) {
        this._editPaperEvent.emit(paper);
    };
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* Input */])(), 
        __metadata('design:type', (typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__models_Paper__["a" /* Paper */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__models_Paper__["a" /* Paper */]) === 'function' && _a) || Object)
    ], PaperDetailsComponent.prototype, "paper", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["T" /* Output */])('editPaperEvent'), 
        __metadata('design:type', Object)
    ], PaperDetailsComponent.prototype, "_editPaperEvent", void 0);
    PaperDetailsComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["U" /* Component */])({
            selector: 'paper-details',
            template: __webpack_require__(629),
            styles: [__webpack_require__(624)]
        }), 
        __metadata('design:paramtypes', [])
    ], PaperDetailsComponent);
    return PaperDetailsComponent;
    var _a;
}());
//# sourceMappingURL=D:/STUDENT/WROCLAW/Semestr_2/Software System Development/implementation/yams/src/PaperDetails.component.js.map

/***/ }),

/***/ 464:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__models_Paper__ = __webpack_require__(145);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__models_PageMode__ = __webpack_require__(96);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PaperEditComponent; });
/**
 * Summary:
 * Component to edit each paper card on the search list
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var PaperEditComponent = (function () {
    function PaperEditComponent() {
        this.changePageMode = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["G" /* EventEmitter */]();
    }
    PaperEditComponent.prototype.backToGeneral = function () {
        //TODO: add cancelation code
        this.changePageMode.emit(__WEBPACK_IMPORTED_MODULE_2__models_PageMode__["a" /* PageMode */].General);
    };
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* Input */])('paper'), 
        __metadata('design:type', (typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__models_Paper__["a" /* Paper */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__models_Paper__["a" /* Paper */]) === 'function' && _a) || Object)
    ], PaperEditComponent.prototype, "paper", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["T" /* Output */])('changePageMode'), 
        __metadata('design:type', Object)
    ], PaperEditComponent.prototype, "changePageMode", void 0);
    PaperEditComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["U" /* Component */])({
            selector: 'paper-edit',
            template: __webpack_require__(630),
            styles: [__webpack_require__(625)]
        }), 
        __metadata('design:paramtypes', [])
    ], PaperEditComponent);
    return PaperEditComponent;
    var _a;
}());
//# sourceMappingURL=D:/STUDENT/WROCLAW/Semestr_2/Software System Development/implementation/yams/src/PaperEdit.component.js.map

/***/ }),

/***/ 465:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__models_PageMode__ = __webpack_require__(96);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_PaperManagement_service__ = __webpack_require__(97);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PaperGeneralComponent; });
/**
 * Summary:
 * Main component for PaperManagement general tab
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var PaperGeneralComponent = (function () {
    function PaperGeneralComponent(_paperMngService) {
        this._paperMngService = _paperMngService;
        this.pageMode = __WEBPACK_IMPORTED_MODULE_1__models_PageMode__["a" /* PageMode */];
    }
    PaperGeneralComponent.prototype.ngOnInit = function () {
        this.currentMode = this.pageMode.General;
        //this._paperMngService.getPapersForReviewHttp();
        this.papersForReview = [
            { id: 1, title: 'Feature selection in machine learning', author: 'Rostyslav Solopatych', summary: 'The best paper in the world', grade: 99, dateUpload: '2017-06-01' },
            { id: 2, title: 'Some random bulshit 1', author: 'Rostyslav 1', summary: 'The best paper in the world', grade: 99, dateUpload: '2017-06-01' },
            { id: 3, title: 'Some random 3 bulshit 2', author: 'Rostyslav 2', summary: 'The best paper in the world', grade: 99, dateUpload: '2017-06-01' },
            { id: 4, title: 'Some random bulshit 3', author: 'Rostyslav 3', summary: 'The best paper in the world', grade: 99, dateUpload: '2017-06-01' },
            { id: 5, title: 'Some random bulshit 4', author: 'Rostyslav 4', summary: 'The best paper in the world', grade: 99, dateUpload: '2017-06-01' },
        ];
    };
    PaperGeneralComponent.prototype.changeMode = function (mode) {
        this.currentMode = mode;
    };
    PaperGeneralComponent.prototype.setPaperToEdit = function (paper) {
        this.paperToEdit = paper;
    };
    PaperGeneralComponent.prototype.reviewPaper = function (paper) {
        this.paperToReview = paper;
        this.changeMode(__WEBPACK_IMPORTED_MODULE_1__models_PageMode__["a" /* PageMode */].Review);
    };
    PaperGeneralComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["U" /* Component */])({
            selector: 'paper-general',
            template: __webpack_require__(631),
            styles: [__webpack_require__(351)]
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__services_PaperManagement_service__["a" /* PaperManagementService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__services_PaperManagement_service__["a" /* PaperManagementService */]) === 'function' && _a) || Object])
    ], PaperGeneralComponent);
    return PaperGeneralComponent;
    var _a;
}());
//# sourceMappingURL=D:/STUDENT/WROCLAW/Semestr_2/Software System Development/implementation/yams/src/PaperGeneral.component.js.map

/***/ }),

/***/ 466:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__models_PageMode__ = __webpack_require__(96);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_PaperManagement_service__ = __webpack_require__(97);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PaperGeneralFrameComponent; });
/**
 * Summary:
 * Logic for PaperManagement general tab
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var PaperGeneralFrameComponent = (function () {
    function PaperGeneralFrameComponent(_paperMngService) {
        this._paperMngService = _paperMngService;
        this.changePageMode = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["G" /* EventEmitter */]();
        this.editPaperEvent = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["G" /* EventEmitter */]();
    }
    PaperGeneralFrameComponent.prototype.ngOnInit = function () {
        // this._paperMngService
        //     .getAllPapersHttp()
        //     .subscribe((papers)=>{
        //         this.papers = papers;
        //     });
        this._papersPrime = [
            { id: 1, title: 'Feature selection in machine learning', author: 'Rostyslav Solopatych', summary: 'The best paper in the world', grade: 99, dateUpload: '2017-06-01' },
            { id: 2, title: 'Some random bulshit 1', author: 'Rostyslav 1', summary: 'The best paper in the world', grade: 99, dateUpload: '2017-06-01' },
            { id: 3, title: 'Some random 3 bulshit 2', author: 'Rostyslav 2', summary: 'The best paper in the world', grade: 99, dateUpload: '2017-06-01' },
            { id: 4, title: 'Some random bulshit 3', author: 'Rostyslav 3', summary: 'The best paper in the world', grade: 99, dateUpload: '2017-06-01' },
            { id: 5, title: 'Some random bulshit 4', author: 'Rostyslav 4', summary: 'The best paper in the world', grade: 99, dateUpload: '2017-06-01' },
        ];
        this.papers = JSON.parse(JSON.stringify(this._papersPrime));
        this.options = {
            paperTitle: '',
            paperAuthor: '',
            dateUpload: ''
        };
    };
    PaperGeneralFrameComponent.prototype.search = function (options) {
        this.papers = JSON.parse(JSON.stringify(this._papersPrime));
        if (options) {
            if (options.paperTitle) {
                this.papers = this.papers.filter(function (paper) { return paper.title.toLowerCase().indexOf(options.paperTitle.toLowerCase()) > -1; });
            }
            if (options.dateUpload) {
                this.papers = this.papers.filter(function (paper) {
                    var d1 = new Date(paper.dateUpload);
                    var d2 = new Date(options.dateUpload);
                    return d1 <= d2;
                });
            }
            if (options.paperAuthor) {
                this.papers = this.papers.filter(function (paper) { return paper.author.toLowerCase().indexOf(options.paperAuthor.toLowerCase()) > -1; });
            }
        }
    };
    PaperGeneralFrameComponent.prototype.addNewPaper = function () {
        this.changePageMode.emit(__WEBPACK_IMPORTED_MODULE_1__models_PageMode__["a" /* PageMode */].Add);
    };
    PaperGeneralFrameComponent.prototype.fireEditPaperEvent = function (paper) {
        this.editPaperEvent.emit(paper);
        this.changePageMode.emit(__WEBPACK_IMPORTED_MODULE_1__models_PageMode__["a" /* PageMode */].Edit);
    };
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["T" /* Output */])('changePageMode'), 
        __metadata('design:type', Object)
    ], PaperGeneralFrameComponent.prototype, "changePageMode", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["T" /* Output */])('editPaperEvent'), 
        __metadata('design:type', Object)
    ], PaperGeneralFrameComponent.prototype, "editPaperEvent", void 0);
    PaperGeneralFrameComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["U" /* Component */])({
            selector: 'paper-general-frame',
            template: __webpack_require__(632),
            styles: [__webpack_require__(351)]
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__services_PaperManagement_service__["a" /* PaperManagementService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__services_PaperManagement_service__["a" /* PaperManagementService */]) === 'function' && _a) || Object])
    ], PaperGeneralFrameComponent);
    return PaperGeneralFrameComponent;
    var _a;
}());
//# sourceMappingURL=D:/STUDENT/WROCLAW/Semestr_2/Software System Development/implementation/yams/src/PaperGeneralFrame.component.js.map

/***/ }),

/***/ 467:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__models_Paper__ = __webpack_require__(145);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__models_PageMode__ = __webpack_require__(96);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_PaperManagement_service__ = __webpack_require__(97);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PaperReviewComponent; });
/**
 * Summary:
 * Component for reviewing paper
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var PaperReviewComponent = (function () {
    function PaperReviewComponent(_paperMngService) {
        this._paperMngService = _paperMngService;
        this.changePageMode = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["G" /* EventEmitter */]();
        //TODO: fix to real files
        this.uploadedFiles = ['Paper file 1', 'Paper file 2'];
    }
    PaperReviewComponent.prototype.ngOnInit = function () {
        //this._paperMngService.getPapersForReviewHttp();
        this.papersForReview = [
            { id: 1, title: 'Feature selection in machine learning', author: 'Rostyslav Solopatych', summary: 'The best paper in the world', grade: 99, dateUpload: '2017-06-01' },
            { id: 2, title: 'Some random bulshit 1', author: 'Rostyslav 1', summary: 'The best paper in the world', grade: 99, dateUpload: '2017-06-01' },
            { id: 3, title: 'Some random 3 bulshit 2', author: 'Rostyslav 2', summary: 'The best paper in the world', grade: 99, dateUpload: '2017-06-01' },
            { id: 4, title: 'Some random bulshit 3', author: 'Rostyslav 3', summary: 'The best paper in the world', grade: 99, dateUpload: '2017-06-01' },
            { id: 5, title: 'Some random bulshit 4', author: 'Rostyslav 4', summary: 'The best paper in the world', grade: 99, dateUpload: '2017-06-01' },
        ];
    };
    PaperReviewComponent.prototype.backToGeneral = function () {
        this.changePageMode.emit(__WEBPACK_IMPORTED_MODULE_2__models_PageMode__["a" /* PageMode */].General);
    };
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* Input */])('paper'), 
        __metadata('design:type', (typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__models_Paper__["a" /* Paper */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__models_Paper__["a" /* Paper */]) === 'function' && _a) || Object)
    ], PaperReviewComponent.prototype, "paper", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["T" /* Output */])('changePageMode'), 
        __metadata('design:type', Object)
    ], PaperReviewComponent.prototype, "changePageMode", void 0);
    PaperReviewComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["U" /* Component */])({
            selector: 'paper-review',
            template: __webpack_require__(633),
            styles: [__webpack_require__(626)]
        }), 
        __metadata('design:paramtypes', [(typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__services_PaperManagement_service__["a" /* PaperManagementService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3__services_PaperManagement_service__["a" /* PaperManagementService */]) === 'function' && _b) || Object])
    ], PaperReviewComponent);
    return PaperReviewComponent;
    var _a, _b;
}());
//# sourceMappingURL=D:/STUDENT/WROCLAW/Semestr_2/Software System Development/implementation/yams/src/PaperReview.component.js.map

/***/ }),

/***/ 468:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `angular-cli.json`.
var environment = {
    production: false
};
//# sourceMappingURL=D:/STUDENT/WROCLAW/Semestr_2/Software System Development/implementation/yams/src/environment.js.map

/***/ }),

/***/ 622:
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ 623:
/***/ (function(module, exports) {

module.exports = ".paperAdd {\n  padding: 20px 50px; }\n  .paperAdd__uploadHistory {\n    list-style: none;\n    box-shadow: black 0 0 10px inset;\n    border: solid black 1px;\n    height: 250px;\n    overflow: auto; }\n    .paperAdd__uploadHistory li {\n      list-style-image: none;\n      list-style-type: none;\n      margin-left: 0; }\n  .paperAdd__dateLbl {\n    font-size: 20px;\n    border: solid 2px #ff3333;\n    padding: 5px;\n    box-shadow: #ff3333 0px 0px 5px inset; }\n"

/***/ }),

/***/ 624:
/***/ (function(module, exports) {

module.exports = "/**\r\n* Summary:\r\n*\r\n*/\n.paperDetails {\n  width: 100%;\n  padding: 10px 30px;\n  margin-bottom: 10px;\n  background-color: #e6e6e6;\n  border-radius: 5px;\n  box-shadow: black 2px 2px 5px; }\n  .paperDetails__icon {\n    height: 100px;\n    width: 100px; }\n"

/***/ }),

/***/ 625:
/***/ (function(module, exports) {

module.exports = ".paperEdit {\n  padding: 20px 50px; }\n  .paperEdit__uploadedFiles {\n    list-style: none;\n    border: solid black 1px;\n    box-shadow: black 0px 0px 3px inset;\n    height: 150px;\n    overflow-y: auto; }\n    .paperEdit__uploadedFiles li {\n      list-style-image: none;\n      list-style-type: none;\n      margin-left: 0; }\n  .paperEdit__btnSet {\n    margin-top: 3em; }\n"

/***/ }),

/***/ 626:
/***/ (function(module, exports) {

module.exports = ".paperReview {\n  padding: 20px 50px; }\n  .paperReview__sendHolder {\n    margin-top: 7.5em; }\n  .paperReview__downloadHolder {\n    margin-top: 1.8em; }\n  .paperReview__papersForReviewList {\n    padding: 10px;\n    list-style: none;\n    border: solid black 1px;\n    box-shadow: black 0px 0px 5px inset; }\n    .paperReview__papersForReviewList li {\n      list-style-image: none;\n      list-style-type: none;\n      margin-left: 0; }\n"

/***/ }),

/***/ 627:
/***/ (function(module, exports) {

module.exports = "<paper-general></paper-general>"

/***/ }),

/***/ 628:
/***/ (function(module, exports) {

module.exports = "<div class=\"paperAdd\">\r\n    <div class=\"row form-group\">\r\n        <div class=\"col-xs-8\">\r\n            <button class=\"btn btn-primary\" (click)=\"backToGeneral()\">\r\n                <span class=\"glyphicon glyphicon-chevron-left\"></span>\r\n                Back to general\r\n            </button>\r\n        </div>\r\n        <div class=\"col-xs-2\">\r\n            <label for=\"date\" class=\"paperAdd__dateLbl\">\r\n                {{currDate | date: 'dd MMM yyyy'}}\r\n            </label>\r\n        </div>\r\n    </div>\r\n    <form #paperAddForm=\"ngForm\" (ngSubmit)=\"addNewPaper(newPaper)\">    \r\n        <div class=\"row form-group\">\r\n            <div class=\"col-xs-3\" [ngClass]=\"{'has-error': paperTopic.value == -1 && paperTopic.touched}\">\r\n                <label for=\"paperTopic\">Paper Topic</label>\r\n                <select name=\"paperTopic\"\r\n                        class=\"form-control\"\r\n                        [(ngModel)]=\"newPaper.topicId\"\r\n                        #paperTopic=\"ngModel\"\r\n                        required>\r\n                    <option value=\"-1\"></option>\r\n                    <option *ngFor=\"let topic of topics\" [value]=\"topic.id\">\r\n                        {{topic.name}}\r\n                    </option>\r\n                </select>\r\n                <p class=\"help-block\" *ngIf=\"paperTopic.value == -1 && paperTopic.touched\">\r\n                    Paper topic is required\r\n                </p>\r\n            </div>\r\n            <div class=\"col-xs-4\" [ngClass]=\"{'has-error': paperTitle.invalid && paperTitle.dirty}\">\r\n                <label for=\"paperTitle\">Paper Title</label>\r\n                <input type=\"text\"\r\n                        class=\"form-control\"\r\n                        placeholder=\"Type official title\"\r\n                        name=\"paperTitle\"\r\n                        [(ngModel)]=\"newPaper.title\"\r\n                        #paperTitle=\"ngModel\"\r\n                        required>\r\n                <p class=\"help-block\" *ngIf=\"paperTitle.invalid && paperTitle.dirty\">\r\n                    Title for paper is required\r\n                </p>\r\n            </div>\r\n        </div>\r\n        <div class=\"row form-group\">\r\n            <div class=\"col-xs-3\">\r\n                <div class=\"row form-group\">\r\n                    <label for=\"paperFile\">Select your paper here</label>\r\n                    <input  #fileInput\r\n                            type=\"file\"\r\n                            name=\"paperFile\"\r\n                            class=\"form-control\"\r\n                            (change)=\"loadFile($event)\">\r\n                </div>\r\n\r\n                <div class=\"row form-group\">\r\n                    <div class=\"col-xs-6\">\r\n                        <button class=\"btn btn-success btn-lg\"\r\n                                type=\"submit\"\r\n                                [disabled]=\"paperAddForm.invalid ||\r\n                                            paperAddForm.pristine ||\r\n                                            paperTopic.value == -1 ||\r\n                                            newPaper.file == null\">\r\n                            <span class=\"glyphicon glyphicon-ok\"></span>\r\n                            SUBMIT\r\n                        </button>\r\n                    </div>\r\n                    <div class=\"col-xs-6\">\r\n                        <div class=\"row form-group text-center\">\r\n                            <button class=\"btn btn-warning\"\r\n                                    type=\"button\"\r\n                                    [disabled]=\"newPaper.file == null\">\r\n                                <span class=\"glyphicon glyphicon-upload\"></span>\r\n                                Upload Paper\r\n                            </button>\r\n                        </div>\r\n                        <div class=\"row form-group text-center\">\r\n                            <button class=\"btn btn-danger\"\r\n                                    type=\"button\"\r\n                                    [disabled]=\"newPaper.file == null\"\r\n                                    (click)=\"removeFile()\">\r\n                                <span class=\"glyphicon glyphicon-remove-sign\"></span>\r\n                                Remove Paper\r\n                            </button>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class=\"col-xs-3\"></div>\r\n            <div class=\"col-xs-3\">\r\n                <label for=\"uploadHistory\">Upload History</label>\r\n                <ul class=\"paperAdd__uploadHistory\">\r\n                    <li *ngFor=\"let str of uploadHistory\">\r\n                        {{str}}\r\n                    </li>\r\n                    <li *ngIf=\"uploadHistory.length == 0\">\r\n                        <h1><i>No history</i></h1>\r\n                    </li>                \r\n                </ul>\r\n            </div>\r\n        </div>\r\n    </form>\r\n</div>"

/***/ }),

/***/ 629:
/***/ (function(module, exports) {

module.exports = "<div class=\"paperDetails\">\r\n    <div class=\"row\">\r\n        <div class=\"col-xs-2\">\r\n            <img class=\"paperDetails__icon\" src=\"/static/assets/images/person.png\" alt=\"\">\r\n        </div>\r\n        <div class=\"col-xs-10\">\r\n            <div class=\"row\">\r\n                <div class=\"col-xs-12\">\r\n                    <h3>\r\n                        {{paper.title}}\r\n                        <button class=\"btn btn-default pull-right\" (click)=\"editPaper(paper)\">\r\n                            Edit Info\r\n                        </button>\r\n                    </h3>                    \r\n                </div>\r\n                <div class=\"col-xs-12\">\r\n                    <span class=\"col-xs-6\">\r\n                        <label>{{paper.author}}</label>\r\n                    </span>\r\n                    <span class=\"col-xs-6\">\r\n                        <i>published {{paper.dateUpload | date: 'MMM dd yyyy'}}</i>\r\n                    </span>\r\n                </div>\r\n            </div>\r\n            <div class=\"row\">\r\n                <div class=\"col-xs-12 col-md-12 col-sm-12\">\r\n                    <textarea [(ngModel)]=\"paper.summary\"\r\n                                name=\"summary\"\r\n                                readonly\r\n                                cols=\"30\"\r\n                                rows=\"2\"\r\n                                class=\"form-control\"\r\n                                placeholder=\"Summary\">\r\n                    </textarea>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ 630:
/***/ (function(module, exports) {

module.exports = "<div class=\"paperEdit\">    \r\n    <div class=\"row form-group\">\r\n        <div class=\"col-xs-3\">\r\n            <button class=\"btn btn-primary\" (click)=\"backToGeneral()\">\r\n                <span class=\"glyphicon glyphicon-chevron-left\"></span>\r\n                Back to general\r\n            </button>\r\n        </div>\r\n        <div class=\"col-xs-3\">\r\n            <button class=\"btn btn-success\" (click)=\"save()\">\r\n                <span class=\"glyphicon glyphicon-floppy-disk\"></span>\r\n                Save\r\n            </button>\r\n        </div>\r\n    </div>\r\n    <div class=\"row form-group\">\r\n        <div class=\"col-xs-3\">\r\n            <label for=\"paperTopic\">Paper Topic</label>\r\n            <select name=\"paperTopic\" class=\"form-control\">\r\n                <option value=\"Science\">Science</option>\r\n            </select>\r\n        </div>\r\n        <div class=\"col-xs-4\">\r\n            <label for=\"paperTitle\">Paper Title</label>\r\n            <input name=\"paperTitle\"\r\n                   [(ngModel)]=\"paper.title\"\r\n                   type=\"text\"\r\n                   class=\"form-control\"\r\n                   placeholder=\"Type official title\">\r\n        </div>\r\n    </div>\r\n    <div class=\"row form-group\">\r\n        <div class=\"col-xs-4\">\r\n            <label for=\"equipment\">\r\n                <input type=\"checkbox\">\r\n                Need extra equipment for presentation\r\n            </label>\r\n        </div>\r\n    </div>\r\n    <div class=\"row form-group\">\r\n        <div class=\"col-xs-8\">\r\n            <label for=\"extraInfo\">Tell us more about your presentation</label>\r\n            <textarea name=\"extraInfo\"\r\n                      [(ngModel)]=\"paper.summary\"\r\n                      cols=\"30\"\r\n                      rows=\"10\"\r\n                      class=\"form-control\"\r\n                      placeholder=\"Describe the kind of equipment and extra info\"></textarea>\r\n        </div>\r\n    </div>\r\n    <div class=\"row form-group\">        \r\n        <div class=\"col-xs-3\">\r\n            <div class=\"paperEdit__btnSet\">\r\n                <div class=\"row form-group text-center\">\r\n                    <button class=\"btn btn-warning\">\r\n                        <span class=\"glyphicon glyphicon-upload\"></span>\r\n                        Upload Paper\r\n                    </button>\r\n                </div>\r\n                <div class=\"row form-group text-center\">\r\n                    <button class=\"btn btn-danger\">\r\n                        <span class=\"glyphicon glyphicon-remove-sign\"></span>\r\n                        Remove Paper\r\n                    </button>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-xs-4\">\r\n            <label for=\"uploadedFiles\">Uploaded Files</label>\r\n            <ul class=\"paperEdit__uploadedFiles\">                \r\n                <li>\r\n                    <h1><i>No history</i></h1>\r\n                </li>                \r\n            </ul>\r\n        </div>\r\n        <div class=\"col-xs-3\">\r\n            <div class=\"paperEdit__btnSet\">\r\n                <button class=\"btn btn-success btn-lg\">\r\n                    <span class=\"glyphicon glyphicon-floppy-disk\"></span>\r\n                    SAVE\r\n                </button>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ 631:
/***/ (function(module, exports) {

module.exports = "<div class=\"paperGeneral\">\r\n    <div class=\"paperGeneral__header\">\r\n        <div class=\"row\">\r\n            <div class=\"col-xs-2 col-md-2 col-sm-2 text-center\">\r\n                <div class=\"col-xs-12\">\r\n                    <img class=\"paperGeneral__icon\" src=\"static/assets/images/paperGeneral.png\" alt=\"\">\r\n                </div>\r\n                <div class=\"col-xs-12 text-center\">\r\n                    <label>YAMS</label>\r\n                </div>\r\n            </div>\r\n            <div class=\"col-xs-6 col-md-6 col-sm-6\">\r\n                <h2>Paper Management</h2>\r\n            </div>\r\n            <div class=\"col-xs-4 col-md-4 col-sm-4\">\r\n                <label class=\"pull-right\">\r\n                    Welcome, Admin <span class=\"glyphicon glyphicon-chevron-left\"></span> Sign out\r\n                </label>\r\n            </div>\r\n        </div>\r\n    </div>\r\n\r\n    <div [ngSwitch]=\"currentMode\">\r\n        <paper-general-frame *ngSwitchCase=\"pageMode.General\" \r\n                             (changePageMode)=\"changeMode($event)\"\r\n                             (editPaperEvent)=\"setPaperToEdit($event)\"></paper-general-frame>\r\n\r\n        <paper-add *ngSwitchCase=\"pageMode.Add\"\r\n                   (changePageMode)=\"changeMode($event)\"></paper-add>\r\n\r\n        <paper-edit *ngSwitchCase=\"pageMode.Edit\"\r\n                    [paper]=\"paperToEdit\"\r\n                    (changePageMode)=\"changeMode($event)\"></paper-edit>\r\n        \r\n        <paper-review *ngSwitchCase=\"pageMode.Review\"\r\n                      [paper]=\"paperToReview\"\r\n                      (changePageMode)=\"changeMode($event)\"></paper-review>\r\n    </div>\r\n\r\n    <div class=\"paperGeneral__reviewPanel\">\r\n        <label>Papers for review</label>\r\n\r\n        <p *ngFor=\"let paper of papersForReview; let i=index;\">\r\n            <a *ngIf=\"i < 4\"\r\n                href=\"#\"\r\n                class=\"paperGeneral__reviewPaper\"\r\n                (click)=\"reviewPaper(paper)\">\r\n                {{paper.title}}\r\n            </a>\r\n        </p>\r\n        <p *ngIf=\"papersForReview.length > 4\">\r\n            4 of {{papersForReview.length}}\r\n        </p>\r\n    </div>\r\n</div>"

/***/ }),

/***/ 632:
/***/ (function(module, exports) {

module.exports = "<div class=\"paperGeneral__frame\">\r\n    <div class=\"paperGeneral__paperFrame\">\r\n        <div class=\"table\">\r\n            <form #searchForm=\"ngForm\" (ngSubmit)=\"search(options)\">\r\n                <div class=\"row form-group\">\r\n                    <div class=\"col-xs-12 col-md-4 col-sm-4 text-center\">\r\n                        <label for=\"title\">Paper Title</label>\r\n                        <div class=\"input-group\">\r\n                            <input [(ngModel)]=\"options.paperTitle\"\r\n                                    type=\"text\"\r\n                                    name=\"paperTitle\"\r\n                                    class=\"form-control\"\r\n                                    placeholder=\"Search\">\r\n                            <span class=\"input-group-addon\">\r\n                                <span class=\"glyphicon glyphicon-search\" aria-hidden=\"true\"></span>\r\n                            </span>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"col-xs-12 col-md-4 col-sm-4 text-center\">\r\n                        <label for=\"date\">Uploaded Before</label>\r\n                        <div class=\"input-group\">\r\n                            <input [(ngModel)]=\"options.dateUpload\"\r\n                                    type=\"date\"\r\n                                    name=\"dateUpload\"\r\n                                    class=\"form-control\"\r\n                                    placeholder=\"Date of upload\">\r\n                            <span class=\"input-group-addon\">\r\n                                <span class=\"glyphicon glyphicon-calendar\" aria-hidden=\"true\"></span>\r\n                            </span>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"col-xs-12 col-md-4 col-sm-4 text-center\">\r\n                        <label for=\"title\">Paper Author</label>\r\n                        <div class=\"input-group\">\r\n                            <input [(ngModel)]=\"options.paperAuthor\"\r\n                                    type=\"text\"\r\n                                    name=\"paperAuthor\"\r\n                                    class=\"form-control\"\r\n                                    placeholder=\"Search\">\r\n                            <span class=\"input-group-addon\">\r\n                                <span class=\"glyphicon glyphicon-search\" aria-hidden=\"true\"></span>\r\n                            </span>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"row form-group\">\r\n                    <div class=\"col-xs-12 col-md-8 col-sm-8 text-center\"></div>\r\n                    <div class=\"col-xs-12 col-md-4 col-sm-4 text-center\">\r\n                        <button class=\"btn btn-primary btn-sm\"\r\n                                type=\"submit\"\r\n                                [disabled]=\"searchForm.pristine\">\r\n                            <span class=\"glyphicon glyphicon-chevron-left\"></span>\r\n                            Search\r\n                        </button>\r\n                    </div>\r\n                </div>\r\n            </form>\r\n\r\n            <div class=\"row form-group\">\r\n                <div class=\"col-xs-12 col-md-12 col-sm-12\">\r\n                    <label for=\"\">Search results</label>\r\n                </div>\r\n                <div class=\"col-xs-12 col-md-12 col-sm-12\">\r\n                    <div class=\"paperGeneral__searchResults\">\r\n                        <div *ngFor=\"let p of papers\">\r\n                            <paper-details [paper]=\"p\" (editPaperEvent)=\"fireEditPaperEvent($event)\"></paper-details>\r\n                        </div>\r\n                        <div class=\"text-center\" *ngIf=\"papers.length == 0\">\r\n                            <h1><i>No results</i></h1>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    \r\n    <div class=\"row form-group\" style=\"margin-top:20px;\">\r\n        <div class=\"col-xs-9 col-md-9 col-sm-9 text-center\">\r\n            <label style=\"font-size:28px;margin-right:10px;\">Add New Paper</label>\r\n            <button (click)=\"addNewPaper()\"\r\n                    class=\"btn btn-success btn-lg\" style=\"border-radius:50%;\">\r\n                <span class=\"glyphicon glyphicon-plus\"></span>\r\n            </button>\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ 633:
/***/ (function(module, exports) {

module.exports = "<div class=\"paperReview\">\r\n    <div class=\"row form-group\">\r\n        <div class=\"col-xs-3\">\r\n            <button class=\"btn btn-primary\" (click)=\"backToGeneral()\">\r\n                <span class=\"glyphicon glyphicon-chevron-left\"></span>\r\n                Back to general\r\n            </button>\r\n        </div>        \r\n    </div>\r\n    <div class=\"row form-group\">\r\n        <div class=\"col-xs-3\">\r\n            <label for=\"uploadedFiles\">Papers for review</label>\r\n            <ul class=\"paperReview__papersForReviewList\">\r\n                <li *ngFor=\"let paper of papersForReview\">\r\n                    {{paper.title}}\r\n                </li>\r\n            </ul>            \r\n        </div>\r\n        <div class=\"col-xs-2\">\r\n            <div class=\"paperReview__downloadHolder\">\r\n                <button class=\"btn btn-warning\">\r\n                    <span class=\"glyphicon glyphicon-download-alt\"></span>\r\n                    Download file\r\n                </button>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-xs-1\">\r\n            <label for=\"mark\">Mark (%)</label>\r\n            <input [(ngModel)]=\"paper.grade\" type=\"number\" class=\"form-control\" min=\"1\" max=\"99\">\r\n        </div>\r\n    </div>\r\n    <div class=\"row form-group\">\r\n        <div class=\"col-xs-6\">\r\n            <label for=\"comment\">\r\n                Add review comment it will be seen by author of the paper\r\n            </label>\r\n            <textarea name=\"comment\"\r\n                    class=\"form-control\"\r\n                    cols=\"30\"\r\n                    rows=\"10\"\r\n                    placeholder=\"Describe what should author change to have paper approved\"></textarea>\r\n        </div>\r\n        <div class=\"col-xs-6\">\r\n            <div class=\"paperReview__sendHolder\">\r\n                <button class=\"btn btn-success btn-lg\">\r\n                    <span class=\"glyphicon glyphicon-ok\"></span>\r\n                    SEND REVIEW\r\n                </button>\r\n            </div>\r\n        </div>\r\n    </div>    \r\n</div>"

/***/ }),

/***/ 643:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(353);


/***/ }),

/***/ 96:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PageMode; });
var PageMode;
(function (PageMode) {
    PageMode[PageMode["General"] = 0] = "General";
    PageMode[PageMode["Add"] = 1] = "Add";
    PageMode[PageMode["Edit"] = 2] = "Edit";
    PageMode[PageMode["Review"] = 3] = "Review";
})(PageMode || (PageMode = {}));
//# sourceMappingURL=D:/STUDENT/WROCLAW/Semestr_2/Software System Development/implementation/yams/src/PageMode.js.map

/***/ }),

/***/ 97:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(296);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PaperManagementService; });
/**
 * Summary:
 * General service to provide all paper management functionality
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


//constants
var URL_ALL_PAPERS = '/api/papers';
var PaperManagementService = (function () {
    function PaperManagementService(_http) {
        this._http = _http;
    }
    PaperManagementService.prototype.getAllPapersHttp = function () {
        return this._http
            .get(URL_ALL_PAPERS)
            .map(function (response) { return response.json(); });
    };
    PaperManagementService.prototype.addNewPaperHttp = function (newPaper) {
        return this._http
            .post(URL_ALL_PAPERS, newPaper)
            .map(function (response) { return response.json(); });
    };
    PaperManagementService = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["c" /* Injectable */])(), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]) === 'function' && _a) || Object])
    ], PaperManagementService);
    return PaperManagementService;
    var _a;
}());
//# sourceMappingURL=D:/STUDENT/WROCLAW/Semestr_2/Software System Development/implementation/yams/src/PaperManagement.service.js.map

/***/ })

},[643]);
//# sourceMappingURL=main.bundle.map