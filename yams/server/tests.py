from django.test import TestCase
from .models import Paper, User, Conference
from .serializers import PaperSerializer, UserSerializer, ConferenceSerializer
from rest_framework.parsers import JSONParser, json

# Create your tests here.


class PageSerializerShould(TestCase):

    def CreateJsonFromObject(self):
        page = Paper(title='tt', text='ttxt', conference=1)

        serializer = ConferenceSerializer(page)
        json = serializer.data
        self.asserIs(json['title'], 'tt')
