from rest_framework import serializers
from .models import Paper, Review, Topic  # , User, Conference

#
# class UserSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = User
#         fields = '__all__'
#
#     def create(self, validated_data):
#         return User.objects.create(**validated_data)
#
#     def update(self, instance, validated_data):
#         instance.name = validated_data.get('name', instance.name)
#         instance.age = validated_data.get('age', instance.age)
#         instance.save()
#         return instance
#
#
# class ConferenceSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = Conference
#         fields = ('pk', 'name', 'address', 'created', 'user')
#
#     def create(self, validated_data):
#         return Conference.objects.create(**validated_data)
#
#     def update(self, instance, validated_data):
#         instance.name = validated_data.get('name', instance.name)
#         instance.address = validated_data.get('address', instance.address)
#         instance.created = validated_data.get('created', instance.created)
#         instance.user = validated_data.get('user', instance.user)
#         instance.save()
#         return instance


class TopicSerializer(serializers.ModelSerializer):
    id = serializers.SerializerMethodField('get_it_method')

    class Meta:
        model = Topic
        fields = ('id', 'name')

    def create(self, validated_data):
        return Topic.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.name = validated_data.get('name', instance.name)
        # instance.conference = validated_data.get('conference', instance.conference)
        instance.save()
        return instance

    def get_it_method(self, obj):
        return obj.pk


class PaperSerializer(serializers.ModelSerializer):
    # author = serializers.StringRelatedField()
    grades = serializers.StringRelatedField(many=True)
    grade = serializers.SerializerMethodField('get_grade_average')
    id = serializers.SerializerMethodField('get_id_method')

    class Meta:
        model = Paper
        fields = ('id', 'topic', 'title', 'author', 'grade', 'summary', 'dateUpload', 'grades', 'file')
        # fields = '__all__'

    def create(self, validated_data):
        return Paper.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.title = validated_data.get('title', instance.title)
        instance.summary = validated_data.get('summary', instance.summary)
        instance.conference = validated_data.get('conference', instance.conference)
        instance.author = validated_data.get('author', instance.author)
        instance.grade = validated_data.get('grade', instance.grade)
        # instance.file = validated_data.get('file', instance.file)
        instance.save()
        return instance

    def get_id_method(self, obj):
        return obj.pk

    def get_grade_average(self, obj):
        reviews = Review.objects.all().filter(paper=obj.pk)
        if len(reviews) == 0 or reviews is None:
            return 0

        sum = 0
        for review in reviews:
            sum = sum + review.mark

        return sum/len(reviews)


class ReviewSerializer(serializers.ModelSerializer):
    # user = serializers.StringRelatedField()

    class Meta:
        model = Review
        fields = ('paper', 'user', 'mark', 'comment')

    def create(self, validated_data):
        return Review.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.paper = validated_data.get('paper', instance.paper)
        instance.user = validated_data.get('user', instance.user)
        instance.mark = validated_data.get('mark', instance.mark)
        instance.comment = validated_data.get('comment', instance.comment)

        instance.save()
        return instance


