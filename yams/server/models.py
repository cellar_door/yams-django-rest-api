from django.db import models
import uuid
from os import rename, path
from django.conf import settings

# class User(models.Model):
#     name = models.CharField(max_length=500)
#     age = models.IntegerField()
#
#     def __str__(self):
#         return self.name
#
#
# class Conference(models.Model):
#     name = models.CharField(max_length=500)
#     address = models.CharField(max_length=500)
#     user = models.ForeignKey(User, on_delete=models.CASCADE)
#     created = models.DateTimeField(auto_now_add=True)
#
#     def __str__(self):
#         return self.name
#
#     class Meta:
#         ordering = ('created',)


class Topic(models.Model):
    # conference = models.ForeignKey(Conference, on_delete=models.CASCADE)
    name = models.CharField(max_length=500)

    def __str__(self):
        return self.name


def get_file_path(instance, filename):
        ext = filename.split('.')[-1]
        filename = "%s.%s" % (uuid.uuid4(), ext)
        return path.join('uploads/papers', filename)


class Paper(models.Model):
    title = models.CharField(max_length=100)
    summary = models.CharField(max_length=10000)
    topic = models.ForeignKey(Topic, on_delete=models.CASCADE)
    author = models.CharField(max_length=100)
    # author = models.ForeignKey(User, on_delete=models.CASCADE)
    grade = models.IntegerField(default=0)
    file = models.FileField(upload_to=get_file_path, blank=True)
    dateUpload = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title



class Review(models.Model):
    paper = models.ForeignKey(Paper, related_name='grades', on_delete=models.CASCADE)
    user = models.CharField(max_length=100)
    # user = models.ForeignKey(User, on_delete=models.CASCADE)
    mark = models.IntegerField()
    comment = models.CharField(max_length=1000, blank=True)

    def __str__(self):
        return str(self.mark)
