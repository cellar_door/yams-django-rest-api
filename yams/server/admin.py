from django.contrib import admin
from .models import Paper, Review, Topic  # User, Conference
# Register your models here.

# admin.site.register(User)
# admin.site.register(Conference)
admin.site.register(Paper)
admin.site.register(Review)
admin.site.register(Topic)