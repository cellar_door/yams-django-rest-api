# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-06-03 20:51
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('server', '0007_auto_20170601_2047'),
    ]

    operations = [
        migrations.CreateModel(
            name='Review',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('mark', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Topic',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=500)),
                ('conference', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='server.Conference')),
            ],
        ),
        migrations.RenameField(
            model_name='paper',
            old_name='text',
            new_name='summary',
        ),
        migrations.RemoveField(
            model_name='paper',
            name='file',
        ),
        migrations.AddField(
            model_name='paper',
            name='author',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='server.User'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='paper',
            name='dateUpload',
            field=models.DateTimeField(auto_now_add=True, default=django.utils.timezone.now),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='paper',
            name='grade',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='review',
            name='paper',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='server.Paper'),
        ),
        migrations.AddField(
            model_name='review',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='server.User'),
        ),
    ]
