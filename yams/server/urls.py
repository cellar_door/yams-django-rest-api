from django.conf.urls import url
from . import views

app_name = 'api'

urlpatterns = [
    url(r'^$', views.IndexViewFlat),
    # url(r'^user/$', views.UserList.as_view()),
    # url(r'^user/(?P<pk>[0-9]+)/$', views.UserDetail.as_view()),
    # url(r'^conferences/$', views.ConferenceList.as_view()),
    # url(r'^conference/(?P<pk>[0-9]+)/$', views.ConferenceDetail.as_view()),
    url(r'^papers/$', views.PaperList.as_view()),
    url(r'^paper/(?P<pk>[0-9]+)/$', views.PaperDetail.as_view()),
    url(r'^paper/(?P<pk>[0-9]+)/(?P<filename>file)/$', views.PaperFile.as_view()),
    url(r'^topics/$', views.TopicList.as_view()),
    url(r'^reviews/$', views.ReviewList.as_view()),
    url(r'^review/paper/(?P<pk>[0-9]+)/$', views.ReviewForPaper.as_view()),
    url(r'^topics/$', views.TopicList.as_view()),
]