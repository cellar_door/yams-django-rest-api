from django.http import HttpResponse, JsonResponse, Http404
from django.views import generic
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render, get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser, FileUploadParser
from rest_framework import status
from .models import Paper, Review, Topic  # , User, Conference
from .serializers import PaperSerializer, ReviewSerializer, TopicSerializer #  , UserSerializer, ConferenceSerializer
from os import remove
from django.template import loader
from django.shortcuts import render_to_response

# class UserList(APIView):
#
#     def get(self, request):
#         users = User.objects.all()
#         serializer = UserSerializer(users, many=True)
#         return Response(serializer.data)
#
#     def post(self, request):
#         data = JSONParser().parse(request)
#         serializer = UserSerializer(data=data)
#         if serializer.is_valid():
#             serializer.save()
#             return JsonResponse(serializer.data, status=201)
#         return JsonResponse(serializer.errors, status=400)
#
#
# class UserDetail(APIView):
#
#     def get(self, request, pk, format=None):
#         user = get_object_or_404(User, pk=pk)
#         serializer = UserSerializer(user)
#         return Response(serializer.data)
#
#     def put(self, request, pk):
#         data = JSONParser().parse(request)
#         user = get_object_or_404(User, pk=pk)
#         serializer = UserSerializer(user, data=data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data)
#         return Response(serializer.errors, status=400)
#
#     def delete(self, request, pk):
#         user = get_object_or_404(User, pk=pk)
#         user.delete()
#         return HttpResponse(status=204)
#
#
# class ConferenceList(APIView):
#
#     def get(self, request, format=None):
#         conferences = Conference.objects.all()
#         serializer = ConferenceSerializer(conferences, many=True)
#         return Response(serializer.data)
#
#     def post(self, request, format=None):
#         data = JSONParser().parse(request)
#         serializer = ConferenceSerializer(data=data)
#         if serializer.is_valid():
#             serializer.save()
#             return JsonResponse(serializer.data, status=201)
#         return JsonResponse(serializer.errors, status=400)
#
#
# class ConferenceDetail(APIView):
#
#     def get(self, request, pk, format=None):
#         conf = get_object_or_404(Conference, pk=pk)
#         serializer = ConferenceSerializer(conf)
#         return Response(serializer.data)
#
#     def put(self, request, pk):
#         data = JSONParser().parse(request)
#         conf = get_object_or_404(Conference, pk=pk)
#         serializer = ConferenceSerializer(conf, data=data)
#         if serializer.is_valid():
#             serializer.save()
#             return JsonResponse(serializer.data)
#         return JsonResponse(serializer.errors, status=400)
#
#     def delete(self, request, pk):
#         conf = get_object_or_404(Conference, pk=pk)
#         conf.delete()
#         return HttpResponse(status=204)


class TopicList(APIView):

    def get(self, request, format=None):
        topics = Topic.objects.all()
        serializer = TopicSerializer(topics, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        data = JSONParser().parse(request)
        serializer = TopicSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201)
        return JsonResponse(serializer.errors, status=400)


class PaperList(APIView):

    def get(self, request, format=None):
        papers = Paper.objects.all()
        serializer = PaperSerializer(papers, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        data = JSONParser().parse(request)
        serializer = PaperSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201)
        return JsonResponse(serializer.errors, status=400)


class PaperDetail(APIView):

    def get(self, request, pk, format=None):
        paper = get_object_or_404(Paper, pk=pk)
        serializer = PaperSerializer(paper)
        return Response(serializer.data)


    def put(self, request, pk):
        data = JSONParser().parse(request)
        paper = get_object_or_404(Paper, pk=pk)
        serializer = PaperSerializer(paper, data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        return JsonResponse(serializer.errors, status=400)

    def delete(self, request, pk):
        paper = get_object_or_404(Paper, pk=pk)
        paper.delete()
        return HttpResponse(status=204)


class PaperFile(APIView):
    parser_classes = (FileUploadParser,)

    def post(self, request, pk, filename, format=None):
        file_obj = request.data['file']
        if file_obj is None:
            return Response(status=404)
        paper = Paper.objects.get(pk=pk)
        if paper.file.name:
            return Response(status=404)

        paper.file = file_obj
        paper.save()
        return Response(status=200)

    def delete(self, request, pk, filename, format=None):
        paper = get_object_or_404(Paper, pk=pk)
        if not paper.file.name:
            return Response(status=404)

        storage = paper.file.storage
        path = paper.file.path
        storage.delete(path)  # delete file from disk

        paper.file = None
        paper.save()
        return Response(status=200)

    def put(self, request, pk, filename, format=None):
        paper = get_object_or_404(Paper, pk=pk)
        if not paper.file.name:
            return Response(status=404)

        file_obj = request.data['file']
        if file_obj is None:
            return Response(status=404)

        storage = paper.file.storage
        path = paper.file.path
        storage.delete(path)  # delete file from disk
        paper.file = None

        paper.file = file_obj
        paper.save()
        return Response(status=200)

class ReviewList(APIView):

    def get(self, request, format=None):
        review = Review.objects.all()
        serializer = ReviewSerializer(review, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        data = JSONParser().parse(request)
        # data.page = data.id
        serializer = ReviewSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201)
        return JsonResponse(serializer.errors, status=400)


class ReviewForPaper(APIView):

    def get(self, request, pk, format=None):
        review = Review.objects.all().filter(paper=pk)
        serializer = ReviewSerializer(review, many=True)
        return Response(serializer.data)

class ReviewDetail(APIView):

    def get(self, request, pk, pk_2, format=None):
        review = get_object_or_404(Review, pk=pk)
        serializer = ReviewSerializer(review)
        return Response(serializer.data)

    def put(self, request, pk, pk_2):
        data = JSONParser().parse(request)
        paper = get_object_or_404(Review, pk=pk)
        serializer = ReviewSerializer(paper, data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        return JsonResponse(serializer.errors, status=400)

    def delete(self, request, pk, pk_2):
        paper = get_object_or_404(Review, pk=pk)
        paper.delete()
        return HttpResponse(status=204)


class PaperUploadView(APIView):
    parser_classes = (FileUploadParser,)

    def post(self, request, filename, paper_pk, format=None):
        file_obj = request.data['file']
        if file_obj is None:
            return Response(status=404)
        paper = Paper.objects.get(pk=paper_pk)
        paper.file = file_obj
        paper.save()
        return Response(status=204)


def IndexViewFlat(request):
   return render_to_response('server/index.html')